package vuelouy.dominio;

import java.net.URL;
import javafx.scene.media.AudioClip;
import java.util.Objects;

/**
 *
 * @author emmanuel
 */
public class Cancion {
    private String nombreCancion;
    private AudioClip archivoMusica;
    
    public Cancion() {
        this.nombreCancion = "";
    }
    
    public Cancion(String unNombre, URL unaUrl) {
        this.nombreCancion = unNombre;
        archivoMusica = new AudioClip(unaUrl.toString());
    }

    public String getNombreCancion() {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }
    
    /**
     *
     * @return el manejador de la pista de audio
     */
    public AudioClip getCancion() {
        return archivoMusica;
    }

    /**
     * Se crea una nueva cancion
     * @param ruta es la direccion donde se encuentra la nueva cancion
     */
    public void setCancion(URL url) {
        archivoMusica = new AudioClip(url.toString());
    }
    
    //Reproduce la cancion this
    public void reproducirCancion() {
        this.archivoMusica.play();        
    }
    
    //Reproduce la cancion this con un volumen predefinido
    public void reproducirCancion(double volumen) {
        this.archivoMusica.play(volumen);
    }
    
    //Detiene la reproduccion de this
    public void detenerCancion() {
        this.archivoMusica.stop();
    }
    
    @Override
    public String toString() {
        return this.nombreCancion;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cancion other = (Cancion) obj;
        if (!Objects.equals(this.nombreCancion, other.nombreCancion)) {
            return false;
        }
        return true;
    }
}