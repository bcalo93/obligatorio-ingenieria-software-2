package vuelouy.dominio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author bcalo
 */
public class LectorArchivos {
    
    public static ArrayList<Cancion> levantarArchivosAudio(String camino) {
        ArrayList<Cancion> retorno = new ArrayList<>();
        try {
            File directorio = new File(camino);
            File[] listaArchivosMusica = directorio.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".mp3") || name.toLowerCase().endsWith(".wav");
                }
            });
            for (int i = 0; i < listaArchivosMusica.length; i++) {
                if (listaArchivosMusica[i].isFile()) {
                    File archivoMusica = listaArchivosMusica[i];
                    try {
                        Cancion cancion = new Cancion(archivoMusica.getName(),
                                archivoMusica.toURI().toURL());
                        retorno.add(cancion);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        
        return retorno;
    }

    public static String leerProperty(String caminoArchivo, String clave) {
        String valor = null;
        Properties prop = new Properties();
        InputStream entrada = null;
        
        try {
            String camino = caminoArchivo + ".properties";
            entrada = new FileInputStream(camino);
            prop.load(entrada);
            valor = prop.getProperty(clave);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (entrada != null) {
                try {
                    entrada.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return valor;
    }
}
