/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vuelouy.dominio;

import java.util.ArrayList;

/**
 * 
 * @author 
 */
public class Musica {

    /**Bandera qeu indica si se esta reproduciendo una pista**/
    private boolean reproduciendo;
    // Lista con canciones reproducibles
    private ArrayList<Cancion> listaCanciones;
    /**Numero de pista que se sesta reproduciendo**/
    private int posActual;
    // Volumen del reproductor de musica
    private double volumen;
    
    /**
     * Constructor de la clase
     */
    public Musica() {
        listaCanciones = new ArrayList<>();
        this.setReproduciendo(false);
        this.posActual = 0;
        this.volumen = 1.0;
    
    }
    
    public Musica(String camino) {
        this.setReproduciendo(false);
        this.posActual = 0;
        this.listaCanciones = LectorArchivos.levantarArchivosAudio(camino);
        this.volumen = 1.0;
    
    }
    /**
     *
     * @return el numero de pista que se esta reproduciendo. (0 = primero)
     */
    
    public int getPosActual() {
        return posActual;
    }
    
    /**
     * Se cambia el numero de la cancion que se reproduce
     * @param unaPosActual es la nueva cancion
     */
    public void setPosActual(int unaPosActual) {
        posActual = unaPosActual;
    }

    /**
     *
     * @return <code>true</code> si se esta reproduciendo
     */
    public boolean estaReproduciendo() {
        return reproduciendo;
    }
    
    private void setReproduciendo(boolean valor) {
        reproduciendo = valor;
    }

     /**
     *
     * @return la lista de rutas de canciones.
     */
    public ArrayList<Cancion> getListaCanciones() {
        return listaCanciones;
    }

    public void setListaCanciones(ArrayList<Cancion> listaCanciones) {
        this.listaCanciones = listaCanciones;
    }
    
    public void agregarCancion(Cancion unaCancion) {
        this.listaCanciones.add(unaCancion);
    }
    
    public void eliminarCancion(Cancion unaCancion) {
        this.listaCanciones.remove(unaCancion);
    }
    
    public boolean existeCancion(Cancion unaCancion) {
        return this.listaCanciones.contains(unaCancion);
    }

    public double getVolumen() {
        return volumen;
    }

    public void setVolumen(double volumen) {
        this.volumen = volumen;
    }
    
    public boolean hayCanciones() {
        return !this.listaCanciones.isEmpty();
    }
    
    /**
     * Inicia la cancion
     */
    public void reproducir() {
        if (hayCanciones()) {
            this.listaCanciones.get(posActual).reproducirCancion(volumen);
            reproduciendo = true;
        }    
    }
    
    public void reproducir(String cancion) {
        Cancion paraReproducir = new Cancion();
        paraReproducir.setNombreCancion(cancion);
        int posAReproducir = this.listaCanciones.indexOf(paraReproducir);
        if (posAReproducir >= 0) {
            posActual = posAReproducir;
            reproducir();
        }
    }
    
    /**
     * Para la cancion
     */
    public void parar() {
        if (hayCanciones()) {
            this.listaCanciones.get(posActual).detenerCancion();
            reproduciendo = false;
        }
    }
    
    
    private void tocarProximaCancion(int posSonando) {
        if (reproduciendo) {
            listaCanciones.get(posSonando).detenerCancion();
            reproducir();
        }
    }
    
    /**
     * Pasa a la siguiente cancion
     */
    public void siguiente() {
        if (hayCanciones()) {
            int sonando = posActual;
            posActual = (posActual + 1) % listaCanciones.size();
            tocarProximaCancion(sonando);
        }
    }
    
    /**
     * Retrocede a la cancion anterior
     */
    public void anterior() {
        if (hayCanciones()) {
            int sonando = posActual;
            posActual = posActual - 1;
            if (posActual < 0) {
                posActual = listaCanciones.size() - 1;
            }
        
            tocarProximaCancion(sonando);
        }
    }
    
    public Cancion cancionActual() {
        if (hayCanciones()) {
            return listaCanciones.get(posActual);
        } else {
            return new Cancion();
        }
    }
}
