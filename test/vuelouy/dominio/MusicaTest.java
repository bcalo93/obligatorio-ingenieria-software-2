package vuelouy.dominio;

import java.net.URL;
import java.util.ArrayList;
import vuelouy.dominio.Musica;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import vuelouy.dominio.Cancion;

/**
 *
 * @author Pablo
 */
public class MusicaTest {
    
    private final String NOMBRE_CANCION_1 = "wav1";
    private final String RUTA_CANCION_1 = "/vuelosuy/archivos/wav1.wav";
    private final String NOMBRE_CANCION_2 = "wav2";
    private final String RUTA_CANCION_2 = "/vuelosuy/archivos/wav2.wav";
    private final String NOMBRE_CANCION_3 = "wav3";
    private final String RUTA_CANCION_3 = "/vuelosuy/archivos/wav3.wav";
    private Musica musica;
    
    public MusicaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    
    @Before
    public void setUp() {
        musica = new Musica();
        URL url1 = this.getClass().getResource(RUTA_CANCION_1);
        Cancion cancion1 = new Cancion(NOMBRE_CANCION_1, url1);
        URL url2 = this.getClass().getResource(RUTA_CANCION_2);
        Cancion cancion2 = new Cancion(NOMBRE_CANCION_2, url2);
        URL url3 = this.getClass().getResource(RUTA_CANCION_3);
        Cancion cancion3 = new Cancion(NOMBRE_CANCION_3, url3);
        musica.agregarCancion(cancion1);
        musica.agregarCancion(cancion2);
        musica.agregarCancion(cancion3);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetPosActual() {
        System.out.println("getPosActual");
        Musica instance = new Musica();
        instance.setPosActual(20);
        int expResult = 20;
        int result = instance.getPosActual();
        assertEquals(expResult, result);
        
    }
    
    @Test
    public void testEstaReproduciendo() {
        System.out.println("estaReproduciendo");
        Musica instance = new Musica();
        assertTrue(!instance.estaReproduciendo());
        
    }
    
    @Test
    public void testGetListaReproduccion() {
        System.out.println("getListaReproduccion");
        ArrayList<Cancion> expResult = new ArrayList<>();
        expResult.add(new Cancion());
        Musica instance = new Musica();
        instance.setListaCanciones(expResult);
        ArrayList<Cancion> result = instance.getListaCanciones();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAgregarCancion() {
        System.out.println("getListaReproduccion");
        Musica instance = new Musica();
        URL url = this.getClass().getResource(RUTA_CANCION_1);
        Cancion cancion = new Cancion("wav1", url);
        assertTrue(!instance.existeCancion(cancion));
        instance.agregarCancion(cancion);
        assertTrue(instance.existeCancion(cancion));
    }
    
    @Test
    public void testEliminarCancion() {
        System.out.println("getListaReproduccion");
        Musica instance = new Musica();
        URL url = this.getClass().getResource(RUTA_CANCION_1);
        Cancion cancion = new Cancion("wav1", url);
        assertTrue(!instance.existeCancion(cancion));
        instance.agregarCancion(cancion);
        assertTrue(instance.existeCancion(cancion));
        instance.eliminarCancion(cancion);
        assertTrue(!instance.existeCancion(cancion));
    }
    
    @Test
    public void testGetVolumen() {
        System.out.println("getVolumen");
        Musica instance = new Musica();
        instance.setVolumen(20.0);
        double expResult = 20.0;
        double result = instance.getVolumen();
        assertEquals(expResult, result, 0.0);
    }
    
    @Test
    public void testHayCancionesSinCanciones() {
        System.out.println("hayCancionesSinCanciones");
        Musica instance = new Musica();
        assertTrue(!instance.hayCanciones());
    }
    
    @Test
    public void testHayCancionesConCanciones() {
        System.out.println("hayCancionesConCanciones");
        Musica instance = new Musica();
        assertTrue(!instance.hayCanciones());
        URL url = this.getClass().getResource(RUTA_CANCION_2);
        Cancion cancion = new Cancion("wav2", url);
        instance.agregarCancion(cancion);
        assertTrue(instance.hayCanciones());
    }
    
    @Test
    public void testReproducirConCanciones() {
        System.out.println("reproducirConCanciones");
        musica.reproducir();
        assertTrue(musica.estaReproduciendo());
    }
    
    @Test
    public void testReproducirSinCanciones() {
        System.out.println("reproducirSinCanciones");
        Musica instance = new Musica();
        instance.reproducir();
        assertTrue(!instance.estaReproduciendo());
    }
    
    @Test
    public void testCancionActualConCanciones() {
        System.out.println("cancionActualConCanciones");
        Cancion actual = musica.cancionActual();
        String result = musica.cancionActual().getNombreCancion();
        assertEquals(NOMBRE_CANCION_1, result);
    }
    
    @Test
    public void testCancionActualSinCanciones() {
        System.out.println("cancionActualSinCanciones");
        Musica instance = new Musica();
        String result = instance.cancionActual().getNombreCancion();
        assertEquals("", result);
    }
    
    @Test
    public void testReproducirCancionExistente() {
        System.out.println("reproducirCancionExistente");
        musica.reproducir(NOMBRE_CANCION_3);
        String result = musica.cancionActual().getNombreCancion();
        assertTrue(musica.estaReproduciendo());
        assertEquals(NOMBRE_CANCION_3, result);
        
    }
    
    @Test
    public void testReproducirCancionNoExistente() {
        System.out.println("reproducirCancionNoExistente");
        musica.reproducir("No existe");
        String result = musica.cancionActual().getNombreCancion();
        assertTrue(!musica.estaReproduciendo());
        assertEquals(NOMBRE_CANCION_1, result);
    }
    
    @Test
    public void testDetenerConCancion() {
        System.out.println("detenerConCancion");
        musica.reproducir();
        assertTrue(musica.estaReproduciendo());
        musica.parar();
        assertTrue(!musica.estaReproduciendo());
    }
    
    @Test
    public void testDetenerSinCancion() {
        System.out.println("detenerSinCancion");
        Musica instance = new Musica();
        assertTrue(!musica.estaReproduciendo());
        instance.parar();
        assertTrue(!musica.estaReproduciendo());
    }
    
    @Test
    public void testSiguienteCancionSinReproducir() {
        System.out.println("siguienteCancionSinReproducir");
        musica.siguiente();
        String result = musica.cancionActual().getNombreCancion();
        assertEquals(NOMBRE_CANCION_2, result);
        assertTrue(!musica.estaReproduciendo());
    }
    
    @Test
    public void testSiguienteCancionReproduciendo() {
        System.out.println("siguienteCancionReproduciendo");
        musica.reproducir();
        assertTrue(musica.estaReproduciendo());
        musica.siguiente();
        String result = musica.cancionActual().getNombreCancion();
        assertEquals(NOMBRE_CANCION_2, result);
        assertTrue(musica.estaReproduciendo());
    }
    
    @Test
    public void testSiguienteCasoBorde() {
        System.out.println("siguienteCasoBorde");
        musica.setPosActual(2);
        musica.siguiente();
        String result = musica.cancionActual().getNombreCancion();
        assertEquals(NOMBRE_CANCION_1, result);
        assertTrue(!musica.estaReproduciendo());
    }
    
    @Test
    public void testSiguienteSinCancion() {
        System.out.println("siguienteSinCancion");
        Musica instance = new Musica();
        instance.siguiente();
        int result = instance.getPosActual();
        assertEquals(0, result);
        assertTrue(!instance.estaReproduciendo());
    }
    
    @Test
    public void testAnteriorCancionSinReproducir() {
        System.out.println("anteriorCancionSinReproducir");
        musica.setPosActual(1);
        musica.anterior();
        String result = musica.cancionActual().getNombreCancion();
        assertEquals(NOMBRE_CANCION_1, result);
        assertTrue(!musica.estaReproduciendo());
    }
    
    @Test
    public void testAnteriorCancionReproduciendo() {
        System.out.println("anteriorCancionReproduciendo");
        musica.setPosActual(1);
        musica.reproducir();
        musica.anterior();
        String result = musica.cancionActual().getNombreCancion();
        assertEquals(NOMBRE_CANCION_1, result);
        assertTrue(musica.estaReproduciendo());
    }
    
    @Test
    public void testAnteriorCasoBorde() {
        System.out.println("anteriorCasoBorde");
        musica.anterior();
        String result = musica.cancionActual().getNombreCancion();
        assertEquals(NOMBRE_CANCION_3, result);
        assertTrue(!musica.estaReproduciendo());
    }
    
    @Test
    public void testAnteriorSinCanciones() {
        System.out.println("anteriorSinCanciones");
        Musica instance = new Musica();
        instance.anterior();
        int result = instance.getPosActual();
        assertEquals(0, result);
        assertTrue(!instance.estaReproduciendo());
    }
}
