/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vuelouy.dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author emmanuel
 */
public class LectorArchivosTest {
    
    private final String CAMINO_ARCHIVO = "test/vuelosuy/archivos/";
    private final String PROPERTY = "test/vuelosuy/archivos/test";
    private final String CLAVE_UNO = "prueba.properties.uno";
    private final String CLAVE_DOS = "prueba.properties.dos";
    
    public LectorArchivosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of levantarArchivosAudio method, of class LectorArchivos.
     */
    @Test
    public void testLevantarArchivosAudio() {
        System.out.println("levantarArchivosAudio");
        Cancion cancion1 = new Cancion();
        cancion1.setNombreCancion("wav1.wav");
        Cancion cancion2 = new Cancion();
        cancion2.setNombreCancion("wav2.wav");
        Cancion cancion3 = new Cancion();
        cancion3.setNombreCancion("wav3.wav");
        ArrayList<Cancion> result = LectorArchivos.levantarArchivosAudio(CAMINO_ARCHIVO);
        assertTrue(result.contains(cancion1));
        assertTrue(result.contains(cancion2));
        assertTrue(result.contains(cancion3));
        assertEquals(3, result.size());
    }
    
    @Test
    public void testLevantarArchivosCaminoIncorrecto() {
        System.out.println("levantarArchivosCaminoIncorrecto");
        ArrayList<Cancion> result = LectorArchivos.levantarArchivosAudio("caminoIncorrecto");
        assertTrue(result.isEmpty());
    }

    /**
     * Test of leerProperty method, of class LectorArchivos.
     */
    @Test
    public void testLeerProperty() {
        System.out.println("leerProperty");
        String expResult = "Valor Numero Uno";
        String result = LectorArchivos.leerProperty(PROPERTY, CLAVE_UNO);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testLeerPropertyCaminoIncorrecta() {
        System.out.println("leerPropertyCaminoIncorrecto");
        String caminoArchivo = "Camino/Incorrecto";
        String expResult = null;
        String result = LectorArchivos.leerProperty(caminoArchivo, CLAVE_DOS);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testLeerPropertyClaveIncorrecta() {
        System.out.println("leerPropertyClaveIncorrecta");
        String claveIncorrecta = "clave.incorrecta";
        String expResult = null;
        String result = LectorArchivos.leerProperty(CAMINO_ARCHIVO, claveIncorrecta);
        assertEquals(expResult, result);
    }
    
}
