package vuelouy.dominio;


import java.net.URL;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author emmanuel
 */
public class CancionTest {
    
    private final String RUTA_CANCION_1 = "/vuelosuy/archivos/wav1.wav";
    private final String RUTA_CANCION_2 = "/vuelosuy/archivos/wav2.wav";
    private final String RUTA_CANCION_3 = "/vuelosuy/archivos/wav3.wav";
    
    public CancionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNombreCancion method, of class Cancion.
     */
    @Test
    public void testGetNombreCancion() {
        System.out.println("getNombreCancion");
        Cancion instance = new Cancion();
        instance.setNombreCancion("Una Cancion");
        String expResult = "Una Cancion";
        String result = instance.getNombreCancion();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCancion method, of class Cancion.
     */
    @Test
    public void testGetCancion() {
        System.out.println("getCancion");
        Cancion instance = new Cancion();
        URL url = this.getClass().getResource(RUTA_CANCION_1);
        instance.setCancion(url);
        //System.out.println("url: " + url.toString());
        String expResult = url.toString();
        String result = instance.getCancion().getSource();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of reproducirCancion method, of class Cancion.
     */
    @Test
    public void testReproducirCancionSinParametros() {
        System.out.println("reproducirCancion");
        URL url = this.getClass().getResource(RUTA_CANCION_2);
        Cancion instance = new Cancion("wav2.wav", url);
        instance.reproducirCancion();
        boolean expResult = true;
        boolean result = instance.getCancion().isPlaying();
        assertEquals(expResult, result);
    }

    /**
     * Test of reproducirCancion method, of class Cancion.
     */
    @Test
    public void testReproducirCancionConParametros() {
        System.out.println("reproducirCancion");
        double volumen = 0.5;
        URL url = this.getClass().getResource(RUTA_CANCION_2);
        Cancion instance = new Cancion("wav2.wav", url);
        instance.reproducirCancion(volumen);
        assertTrue(instance.getCancion().isPlaying());
    }

    /**
     * Test of detenerCancion method, of class Cancion.
     */
    @Test
    public void testDetenerCancion() {
        System.out.println("detenerCancion");
        URL url = this.getClass().getResource(RUTA_CANCION_2);
        Cancion instance = new Cancion("wav2.wav", url);
        instance.reproducirCancion();
        assertTrue(instance.getCancion().isPlaying());
        instance.detenerCancion();
        assertTrue(!instance.getCancion().isPlaying());
    }

    /**
     * Test of toString method, of class Cancion.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Cancion instance = new Cancion();
        instance.setNombreCancion("CancionTest");
        String expResult = "CancionTest";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Cancion.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        String nombre = "Cancion Igual";
        Cancion cancion1 = new Cancion();
        Cancion cancion2 = new Cancion();
        cancion1.setNombreCancion(nombre);
        cancion2.setNombreCancion(nombre);
        assertTrue(cancion1.equals(cancion2));
    }
    
    @Test
    public void testNotEquals() {
        System.out.println("NotEquals");
        Cancion cancion1 = new Cancion();
        Cancion cancion2 = new Cancion();
        cancion1.setNombreCancion("Nombre");
        cancion2.setNombreCancion("Nombre diferente");
        assertTrue(!cancion1.equals(cancion2));
    }
    
    @Test
    public void testNullEquals() {
        System.out.println("NotEquals");
        Cancion cancion = new Cancion();
        Object obj = null;
        cancion.setNombreCancion("Nombre");
        assertTrue(!cancion.equals(obj));
    }
    
    @Test
    public void testClassEquals() {
        System.out.println("ClassEquals");
        Cancion cancion = new Cancion();
        String cadena = "Una cadena";
        cancion.setNombreCancion("Nombre");
        assertTrue(!cancion.equals(cadena));
    }
}
